<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route:: get('/username/{name?}/{surname?}',function($name=null,$surName=null){
    return "Hi User ". $name . "With surname ".$surName;
    });

Route:: get('/product/{id?}',function($id=null){
    return "id writen is " .$id ;
});
Route:: match(['get','post','put'],'/students',function(Request $req){
    return 'Requested Method is' . $req->method();
});

Route::any('/anycheck',function( Request $req){
    return "Requested Method is: ". $req->method();
});