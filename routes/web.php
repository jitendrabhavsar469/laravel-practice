<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\FluentController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\App;
Use App\PaymentGateway\Payment;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });

Route::get('/username/{name?}',function($name = null ){ return "Hi Users". $name; });

Route::get('/home/{name?}/{age?}',[HomeController::class,'index'])->name('Home.index');

Route::get('/posts/jsonpost',[ClientController::class,'getJson'])->name('postsall.json');

Route::get('/posts/jsonpost/{id}',[ClientController::class,'getJsonById'])->name('posts.json');

Route::get('/fluent',[FluentController::class,'index'])->name('fluent.index');

Route::get('/login',[FormController::class,'index'])->name('login.index');

Route::post('/login',[FormController::class,'login'])->name('validation.index');

Route::get('/session',[SessionController::class,'getSessionData'])->name('getsessiondata.index');

Route::get('/session/save',[SessionController::class,'storeSession'])->name('storesessiondata.index');

Route::get('/session/remove',[SessionController::class,'removeSession'])->name('removesessiondata.index');

Route:: get('/posts',[PostController::class,'fetchAllPosts'])->name('fetchall.index');

Route:: get('/create-post',[PostController::class,'createPost'])->name('createpost.index');

Route:: post('/create-post',[PostController::class,'addPostSubmit'])->name('addpost.index');

Route:: get('/single-post/{id}',[PostController::class,'getPostById'])->name('postbyid.index');

Route::get('/delete-post/{id}',[PostController::class,'deletePost'])->name('deletePost.index');

Route::get('/update-post/{id}',[PostController::class,'updatePost'])->name('update.index');

Route::post('/update-post',[PostController::class,'saveUpdate'])->name('saveupdate.index');

Route::get('/join',[PostController::class,'innerjoin'])->name('join.index');

Route::get('/test-layout',function(){
    return view('test-layout');
});

Route::get('/upload',[UploadController::class,'uploadForm'])->name('uploadfile.index');

Route::post('/upload',[UploadController::class,'uploadFile'])->name('savefile.save');

Route::get('/{locale}',function($locale){
    App::setLocale($locale);
    return view('welcome');
});

Route::get('/faf/facade',function(){
    return Payment::process();
});

Route::get('/email/sendmail-test',[EmailController::class,'sendEmail']);
