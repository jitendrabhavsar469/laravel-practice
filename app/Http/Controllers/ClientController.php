<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ClientController extends Controller
{
    public function getJson(){
        $response = Http::get("http://jsonplaceholder.typicode.com/posts");
        return $response->json();
    }

    public function getJsonById($id){
        $response = http::get("http://jsonplaceholder.typicode.com/posts/".$id);
        return $response->json();
    }
}
