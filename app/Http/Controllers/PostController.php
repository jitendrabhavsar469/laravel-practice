<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Post;

class PostController extends Controller
{
    public function fetchAllPosts()
    {

        // $posts = DB::table('posts')->paginate(20);
        $posts = Post::paginate(20);
        // dd($posts);
        // ->join('posts','users.id','=' ,'posts.user_id')->select('users.name','posts.title','posts.body','posts.id')
        // ->paginate(20);
        return view('posts',compact('posts'));

    }

    public function createPost()
    {
        return view('create-post');
    }

    public function addPostSubmit(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required|min:2',
            'body'=>'required|min:8'
        ]);
        if($validateData){
        DB::table('posts')->insert([
            'title' => $request->title,
            'body' => $request->body
        ]);
        return back()->with('post_created','Post has been added seccesfully');
    }
    }
     public function getPostById($id)
     {

        $post = DB::table('posts')->where('id',$id)->first();
        return view('single-post',compact('post'));

     }

     public function deletePost($id)
     {
        DB::table('posts')->where('id',$id)->delete();
        return back()->with('post_deleted','Post has been deleted successfully');
     }

     public function updatePost($id)
     {
        $post = DB::table('posts')->where('id',$id)->first();
        return view('update-post',compact('post'));
     }

     public function saveUpdate(Request $request)
     {
        $validateData = $request->validate([
            'title' => 'required|min:2',
            'body'=>'required|min:8'
        ]);
        if($validateData){
        $post = DB::table('posts')->where('id',$request->id)->update([
            'title' => $request->title,
            'body' => $request->body
        ]);

        return back()->with('post_updated','post has been updated successfully');
    }
     }

     public function innerJoin()
     {
        $result = DB::table('users')
        ->join('posts','users.id','=' ,'posts.user_id')->select('users.name','posts.title')
        ->get();
        return $result;
     }
}
