<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function getSessionData(Request $request)
    {

        if($request->session()->has('name')){

            echo "Session Exists having name: " .$request->session()->get('name')." and number: ".$request->session()->get('num');
        }
        else{
            echo "Session does not Exists";
         }
    }    

    public function storeSession(Request $request)
    {

        $request->session()->put('name','Jitendra');
        $request->session()->put('num','12344556');
        echo "Data added to session";
    }

    public function removeSession(Request $request)
    {

        $request->session()->forget('name');
        echo "Session has been removed"; 
    }
         
}
