<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FluentController extends Controller
{
    public function index()
    {
        echo "<h1> Fluent Strings</h1>";
        $slice = str::of('Welcome to page')->after('Welcome');
        echo $slice."<br>";
        $sliced = str::of($slice)->afterLast('a');
        echo $sliced."<br>";
        echo (str::of('Hii')->append('hello'))."<br>";
        echo (str::of('hii there')->title())."<br>";
        echo (str::of('hii there')->upper())."<br>";
        echo (str::of('hII THERre')->lower())."<br>";
    }
}
