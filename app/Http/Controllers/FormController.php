<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(){
        return view('login');
    }
    public function login(Request $req){

        $validateData = $req->validate([
            'email' => 'required|email',
            'password'=>'required|min:8'
        ]);

        return $req->all();
    }
}
