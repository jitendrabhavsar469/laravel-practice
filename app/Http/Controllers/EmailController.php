<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    //

    public function sendEmail(){

        $details = [
            'title' => 'Email Title',
            'body' => 'Email Body',

        ];
        Mail::to("jitendrabhavsar469@gmail.com")->send(new TestMail($details));
        return "Mail Sent";
    }
}
