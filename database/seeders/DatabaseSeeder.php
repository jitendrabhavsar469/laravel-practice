<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){

     $faker = Faker::create();
     foreach(range(0,1000) as $index){

        DB::table('posts')->insert([
            'title' => $faker->name(4),
            'body'=> $faker->paragraph(2),
            'user_id'=> rand(1,3)
        ]);
     }

    }

    // public function run()
    // {
    //     // \App\Models\User::factory(10)->create();
    // }
}
