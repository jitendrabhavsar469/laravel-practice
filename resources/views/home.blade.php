<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home-Laravel</title>
</head>
<body>
    <x-Header name={{$firstname}} />
    <h1>
        Welcome! {{$firstname}}{{$userage}}
    </h1>
</body>
</html>