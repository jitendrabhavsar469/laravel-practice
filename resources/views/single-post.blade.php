<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <title>Create Post</title>
</head> 
<body>

    
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        Add new post
                    </div>
                    <div class="card-body">
                       
                        <form>
                        
                            <div class="form-group">
                                <label for="title">Post Title:</label>
                                <input type="text" value="{{$post->title}}" class="form-control" placeholder="Post Title" name="title" id="title" readonly>

                            </div>
                            <div class="form-group">
                                <label for="body">Post Content:</label>
                                <textarea type="text"  rows="3" name="body" id="body" class="form-control" readonly>{{$post->body}}</textarea>
                            </div>
                            


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>