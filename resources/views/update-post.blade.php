<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <title>Edit Post</title>
</head> 
<body>

    
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>Edit post</h2>
                    </div>
                    <div class="card-body">
                        @if(Session::has('post_updated'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('post_updated')}}
                        </div>
                        @endif
                        <form method="POST" action="{{route('saveupdate.index')}}">
                            @csrf
                            <div class="form-group">
                                
                                <input type="hidden" value="{{$post->id}}" class="form-control" placeholder="Post Title" name="id" id="id">

                            </div>
                            <div class="form-group">
                                <label for="title">Post Title:</label>
                                <input type="text" value="{{$post->title}}" class="form-control" placeholder="Post Title" name="title" id="title">
                                @error('title')
                                    <span class="text-danger">{{$message}}</span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label for="body">Post Content:</label>
                                <textarea type="text" rows="3" name="body" id="body" class="form-control">{{$post->body}}</textarea>
                                @error('body')
                                    <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <input type="submit"  class= "btn btn-success mt-5" value="Update">


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>